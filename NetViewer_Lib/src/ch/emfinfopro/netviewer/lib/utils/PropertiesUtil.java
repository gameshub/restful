/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.netviewer.lib.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author BarthA
 */
public class PropertiesUtil {

    
    /**
     * Loads the propteries from a file.
     * @param filePath Path to the file
     * @return a java.util.Properties when successfull, null otherwise
     */
    public static Properties loadProperties(String filePath) {
        Properties properties = new Properties();
        try {
            InputStream is = new FileInputStream(new File(filePath));
            properties.load(is);
        } catch (IOException ex) {
            properties = null;
        }
        return properties;
    }
}
