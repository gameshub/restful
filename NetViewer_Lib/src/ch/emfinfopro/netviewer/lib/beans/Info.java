package ch.emfinfopro.netviewer.lib.beans;

import java.io.Serializable;

/**
 *
 * @author SchnieperN
 */
public class Info implements Serializable {
    
    private String clientVersion;
    private String serverVersion;
    private String contactMail;

    public Info(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public Info(String serverVersion, String contactMail) {
        this.serverVersion = serverVersion;
        this.contactMail = contactMail;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }
    
}
