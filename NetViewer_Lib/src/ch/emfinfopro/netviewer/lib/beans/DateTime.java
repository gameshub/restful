package ch.emfinfopro.netviewer.lib.beans;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author SchnieperN
 */
public class DateTime extends Date implements Serializable {
    
    public DateTime(){ }
    
    public DateTime(Date date) {
        this.setTime(date.getTime());
    }
    
    public DateTime(long date) {
        super(date);
    }
    
}
