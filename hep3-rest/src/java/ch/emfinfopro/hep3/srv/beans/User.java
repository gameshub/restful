/*
 * Erwan Sturzenegger
 */
package ch.emfinfopro.hep3.srv.beans;

/**
 *
 * @author Erwan Sturzenegger
 */
public class User {
    private String email;
    private String role;
    private int roleId;
    private int userId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    
}
