/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author KollyF01
 */
public class Classe implements Serializable {
    private static final long serialVersionUID = 2L;
    private Integer id;
    private String nom;
    private ArrayList<Eleve> lstEleves;
    private String degre;
    private String uuid;
    private String padId;

    public Classe() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ArrayList<Eleve> getLstEleves() {
        return lstEleves;
    }

    public void setLstEleves(ArrayList<Eleve> lstEleves) {
        this.lstEleves = lstEleves;
    }

    public String getDegre() {
        return degre;
    }

    public void setDegre(String degre) {
        this.degre = degre;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPadId() {
        return padId;
    }

    public void setPadId(String padId) {
        this.padId = padId;
    }
    
    
    
}
