/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

/**
 *
 * @author KollyF01
 */

public class ErrorCode {
    enum Errors {
        NOT_FOUND, WRONG_PASSWORD, DATABASE_DUPLICATE, INVALID_ENTRY, TOO_MUCH_ENTRY, BAD_TIMING
    }
    
    public static final int NOT_FOUND = 404;
    public static final int WRONG_PASSWORD = 401;
    public static final int DATABASE_DUPLICATE = 403;
    public static final int INVALID_ENTRY = 405;
    public static final int TOO_MUCH_ENTRY = 406;
    public static final int BAD_TIMING = 408;
}
