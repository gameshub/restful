/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;

/**
 *
 * @author teixeiraT01
 */
public class Log implements Serializable {
    private static final long serialVersionUID = 2L;
    private int id;
    private String nom;
    private String date;

    public Log() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
