/*
 * Erwan Sturzenegger
 */
package ch.emfinfopro.hep3.srv.beans;

import java.util.ArrayList;

/**
 *
 * @author Erwan Sturzenegger
 */
public class Statistiques {
    //Integer car il est nullable
    private Integer partieCommence, partieTermine, score0, score1, score2;
    private String jeu;
    private ArrayList<Integer> tags;

    public Integer getPartieCommence() {
        return partieCommence;
    }

    public void setPartieCommence(Integer partieCommence) {
        this.partieCommence = partieCommence;
    }

    public Integer getPartieTermine() {
        return partieTermine;
    }

    public void setPartieTermine(Integer partieTermine) {
        this.partieTermine = partieTermine;
    }

    public Integer getScore0() {
        return score0;
    }

    public void setScore0(Integer score0) {
        this.score0 = score0;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    public String getJeu() {
        return jeu;
    }

    public void setJeu(String jeu) {
        this.jeu = jeu;
    }

    public ArrayList<Integer> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Integer> tags) {
        this.tags = tags;
    }
    
    public void addTag(int tag){
        if(this.tags == null)
            this.tags = new ArrayList<>();
        this.tags.add(tag);
    }
}
