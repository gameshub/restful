/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.util.Date;

/**
 *
 * @author kollyf01
 */
public class HistoriqueLine {

    private Date date;
    private Date start;
    private Date end;
    private int id;
    private int exerciceId;
    private int parcoursId;
    private int evaluation;
    private int status;
    private int pk_jeu;
    private String nom_jeu;
    private int pk_discipline;
    private String nom_discipline;
    private int harmos;

    public HistoriqueLine() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
        
    public int getExerciceId() {
        return exerciceId;
    }
    public void setExerciceId(int exerciceId) {
        this.exerciceId = exerciceId;
    }
    
    public int getParcoursId() {
        return parcoursId;
    }
    public void setParcoursId(int parcoursId) {
        this.parcoursId = parcoursId;
    }
        
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }
    
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPk_jeu() {
        return pk_jeu;
    }

    public void setPk_jeu(int pk_jeu) {
        this.pk_jeu = pk_jeu;
    }

    public String getNom_jeu() {
        return nom_jeu;
    }

    public void setNom_jeu(String nom_jeu) {
        this.nom_jeu = nom_jeu;
    }

    public int getPk_discipline() {
        return pk_discipline;
    }

    public void setPk_discipline(int pk_discipline) {
        this.pk_discipline = pk_discipline;
    }

    public String getNom_discipline() {
        return nom_discipline;
    }

    public void setNom_discipline(String nom_discipline) {
        this.nom_discipline = nom_discipline;
    }

    public int getHarmos() {
        return harmos;
    }

    public void setHarmos(int harmos) {
        this.harmos = harmos;
    }

}
