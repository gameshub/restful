/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

/**
 *
 * @author KollyF01
 */
public class Langue {

    private int pk_langue;
    private String nom;
    private String abbr;

    public Langue() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public int getPk_langue() {
        return pk_langue;
    }

    public void setPk_langue(int pk_langue) {
        this.pk_langue = pk_langue;
    }

}
