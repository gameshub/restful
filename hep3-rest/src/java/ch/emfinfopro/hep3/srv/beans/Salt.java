/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.codec.digest.DigestUtils;

public class Salt {

    private final static String PASSWORD_P1 = "fI3uv2wAidBtgMxh9pUT";
    private final static String PASSWORD_P2 = "543uG24BRZgDiMLU52hR";

    public static String getSalt() {
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        String day = formatter.format(new Date());
        return encrypt(PASSWORD_P1 + day + PASSWORD_P2);
    }
    
    public static boolean compare(String salt){
        return getSalt().equals(salt);
    }

    public static String encrypt(String msg) {
      return DigestUtils.md5Hex(msg);
    }
}
