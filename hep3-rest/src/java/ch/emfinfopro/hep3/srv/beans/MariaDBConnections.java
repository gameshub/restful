/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;
/**
 *
 * @author myadmin
 */
public class MariaDBConnections {
    
/**********************  PLATEFORME DE PROD *************************/
    private static final String HOST = "hep3.emf-infopro.ch";
    private static final String DBNAME = "hep3_main";
    private static final int PORT = 3306;
    private static final String USERNAME = "hep3_admin";
    private static final String PASSWORD = "asgU1WEZQ2!]a!";
    private static final String DRIVER = "org.mariadb.jdbc.Driver";
    private static final String DBURL = "jdbc:mariadb://"+HOST+":"+PORT+"/"+DBNAME;

/**********************  PLATEFORME DE TEST *************************/    
//    private static final String HOST = "hep3.emf-infopro-test.ch";
//    private static final String DBNAME = "hep3test_main";
//    private static final int PORT = 3306;
//    private static final String USERNAME = "hep3test_admin";
//    private static final String PASSWORD = "fCFc1TSkG~)t";
//    private static final String DRIVER = "org.mariadb.jdbc.Driver";
//    private static final String DBURL = "jdbc:mariadb://"+HOST+":"+PORT+"/"+DBNAME;

/**********************  PLATEFORME DE DÉV *************************/    
//    private static final String HOST = "hep3.emf-infopro-test.ch";
//    private static final String DBNAME = "hep3dev_main";
//    private static final int PORT = 3306;
//    private static final String USERNAME = "hep3dev_user";
//    private static final String PASSWORD = "Dc)6NX_Un{9;";
//    private static final String DRIVER = "org.mariadb.jdbc.Driver";
//    private static final String DBURL = "jdbc:mariadb://"+HOST+":"+PORT+"/"+DBNAME;

/**********************  PLATEFORME LOCALE *************************/    
//    private static final String HOST = "localhost";
////    private static final String DBNAME = "hep3_main"; 
//    private static final String DBNAME = "hep3test_main"; 
//    private static final int PORT = 3306;
//    private static final String USERNAME = "root";
//    private static final String PASSWORD = "";
//    private static final String DRIVER = "org.mariadb.jdbc.Driver";
//    private static final String DBURL = "jdbc:mariadb://"+HOST+":"+PORT+"/"+DBNAME;
    
    public String getUSERNAME() {
        return USERNAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public String getDBURL() {
        return DBURL;
    }
    
    public String getDRIVER() {
        return DRIVER;
    }
}
