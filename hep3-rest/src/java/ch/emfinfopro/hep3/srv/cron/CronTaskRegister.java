/*
 * Erwan Sturzenegger
 */
package ch.emfinfopro.hep3.srv.cron;

import ch.emfinfopro.hep3.srv.cron.task.TaskSupprimerClasses;
import ch.emfinfopro.hep3.srv.cron.task.TaskSupprimerExercices;
import java.util.Calendar;
import java.util.Timer;

/**
 *
 * @author Erwan Sturzenegger
 */
public class CronTaskRegister {

    private static Timer timer;

    private static final int ONE_DAY = 86400000;

    private static boolean registered = false;

    /**
     * Enregistrement des tâches plannifées.
     *
     * Cette méthode enregistre les tâches plannifiées une seul fois par
     * démarrage sur serveur.
     */
    public static void register() {
        if (!registered) {
            registered = true;

            timer = new Timer("CronTask");

            /*
            
            Suppression des exercices non validés de plus de 50 jours. Cette 
            tâche se lance une fois par jour et supprimer tout les exercices 
            ainsi que les évaluations qui y sont relatives.
            
            Le serveur log l'exécution de la tâche ainsi que le nombre d'exercices 
            supprimés
            
             */
            timer.scheduleAtFixedRate(new TaskSupprimerExercices(), 0, ONE_DAY);

            /*
            
            Suppression des classes, des élèves et de toutes les données leur 
            étant relative le 30 juillet à 11h59
            
            La tâche se lance pour le première fois le 30.07.xxxx et ensuite, 
            si le serveur n'est pas redémarrer entre temps, la tâche se lance 
            une fois par jour et ne s'exécute uniquement si le jour est le 
            30.07.xxxx. La tâche doit se lancer une fois par jour car un timer 
            qui s'exécute une fois par année ne pourrait pas prendre en compte 
            les années bissextiles et le timer serait donc faussé.
            
            Le serveur log l'exécution de la tâche.
            
             */
            
            // Désactivation de la suppression annuelle des classes et des élèves
            /*
            Calendar delClasses = Calendar.getInstance();
            //La numérotation des mois commence à 0
            delClasses.set(Calendar.MONTH, 6);
            
            delClasses.set(Calendar.DATE, 30);
            delClasses.set(Calendar.HOUR, 11);
            delClasses.set(Calendar.MINUTE, 59);
            delClasses.set(Calendar.SECOND, 0);
            delClasses.set(Calendar.MILLISECOND, 0);
            if (delClasses.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
                delClasses.add(Calendar.YEAR, 1);
            }
            timer.schedule(new TaskSupprimerClasses(), delClasses.getTime(), ONE_DAY);
             */
        }
    }
}
