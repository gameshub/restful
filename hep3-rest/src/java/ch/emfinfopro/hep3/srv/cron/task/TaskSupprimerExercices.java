/*
 * Erwan Sturzenegger
 */
package ch.emfinfopro.hep3.srv.cron.task;

import ch.emfinfopro.hep3.srv.beans.ConnexionPool;
import ch.emfinfopro.hep3.srv.wrk.WrkDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TimerTask;

/**
 *
 * @author Erwan Sturzenegger
 */
public class TaskSupprimerExercices extends TimerTask{

    public TaskSupprimerExercices() {
        System.out.println("[CRONTASK] Suppression des exercices non validés de plus de 50 jours tous les jours : Tâche plannifiée");
    }

    
    @Override
    public void run() {
        WrkDB db = ConnexionPool.getInstance().getWrkDB();
        Connection conn = db.open();
        try {
            int count = db.delete(conn, "call deleteOldExercices()");
            System.out.println("[CRONTASK] Suppression des exercices non validés de plus de 50 jours tous les jours : Suppression de " + count +" exercices");
        } catch (SQLException ex) {
            System.out.println("[CRONTASK] Suppression des exercices non validés de plus de 50 jours tous les jours : Erreur d'exécution -> " + ex.getMessage());
            try {
                db.rollback(conn);
            } catch (SQLException ex1) {
            }
        } finally { 
            db.close(conn);
        }
    }
    
}
