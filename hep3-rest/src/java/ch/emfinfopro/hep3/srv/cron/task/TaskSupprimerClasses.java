/*
 * Erwan Sturzenegger
 */
package ch.emfinfopro.hep3.srv.cron.task;

import ch.emfinfopro.hep3.srv.beans.ConnexionPool;
import ch.emfinfopro.hep3.srv.wrk.WrkDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimerTask;

/**
 *
 * @author Erwan Sturzenegger
 */
public class TaskSupprimerClasses extends TimerTask {

    public TaskSupprimerClasses() {
        System.out.println("[CRONTASK] Suppression des classes et des élèves de l'années : Tâche plannifée");
    }

    @Override
    public void run() {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        if (day == 30 && month == 7) {
            WrkDB db = ConnexionPool.getInstance().getWrkDB();
            Connection conn = db.open();
            try {
                db.delete(conn, "call deleteData()");
                db.commit(conn);
                System.out.println("[CRONTASK] Suppression des classes et des élèves de l'année : Suppression de toutes les données des élèves");
            } catch (SQLException ex) {
                System.out.println("[CRONTASK] Suppression des classes et des élèves de l'année : Erreur d'exécution -> " + ex.getMessage());
                try {
                    db.rollback(conn);
                } catch (SQLException ex1) {
                }
            } finally {
                
                db.close(conn);
            }
        }

    }

}
