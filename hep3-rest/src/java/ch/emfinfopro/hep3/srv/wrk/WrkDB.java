/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.wrk;

import ch.emfinfopro.hep3.srv.beans.MariaDBConnections;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

/**
 *
 * @author SchnieperN
 */
public class WrkDB {

    private String DBURL;
    private String DBUSERNAME;
    private String DBPASSWORD;
    private String DBDRIVER;

    private String errDB;
    private DataSource ds;

    /**
     * Constructeur du WrkDB, crée le pool de connexion
     */
    public WrkDB() {
        getConfigDB();
        PoolProperties p = new PoolProperties();
        p.setUrl(DBURL);
        p.setDriverClassName(DBDRIVER);
        p.setUsername(DBUSERNAME);
        p.setPassword(DBPASSWORD);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false); // Indique si les objets seront validés après leur retour dans le pool.
        p.setValidationInterval(30000); // Éviter la validation excédentaire, exécuter la validation au maximum à cette fréquence - temps en millisecondes.
        p.setTimeBetweenEvictionRunsMillis(30000); // Temps entre runs of the idle connection validation, abandoned cleaner and idle pool resizing.
        p.setMaxActive(10); // Nbr max de connexions pouvant être alloué en même temps sur le pool
        p.setMaxIdle(4); // Nbr max de connexions idle qui doivent être gardé dans le pool
        p.setInitialSize(5); // Nbr connexions établis lorsque le pool est créée
        p.setMaxWait(10000); // Temps max que le pool attendra le retour d'une connexion avant de générer une exception
        p.setRemoveAbandonedTimeout(60); // Temps avant qu'une connexion puisse être considérée comme abandonnée
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(3); // Nbr minimum de connexion idle qui devraient toujours rester dans le pool
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        ds = new DataSource();
        ds.setPoolProperties(p);
    }

    /**
     * Récupère la configuration de la DB
     */
    private void getConfigDB() {
        MariaDBConnections mdb = new MariaDBConnections();
        DBURL = mdb.getDBURL();
        DBUSERNAME = mdb.getUSERNAME();
        DBPASSWORD = mdb.getPASSWORD();
        DBDRIVER = mdb.getDRIVER();
    }

    /**
     * Récupére une instance de connexion de la pool de connexions.
     *
     * @return la connexion si tout s'est bien passé, sinon null
     */
    public Connection open() {
        Connection conn = null;
        try {
            conn = ds.getConnection();
        } catch (SQLException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            errDB = exceptionAsString;
            conn = null;
        }
        return conn;
    }

    /**
     * Retourne l'instance de connexion au pool de connexions.
     *
     * @param conn
     * @return true si tout s'est bien passé, sinon false
     */
    public boolean close(Connection conn) {
        boolean ok = false;
        try {
            if (conn != null) {
                if (!conn.isClosed()) {
                    conn.close();
                    ok = true;
                }
            }
        } catch (SQLException ex) {
            errDB = ex.getMessage();
        }
        return ok;
    }

    public ResultSet select(Connection conn, String query, Object... o) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        return preparePreparedStatement(ps, o).executeQuery();
    }

    public ResultSet insert(Connection conn, String query, Object... o) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparePreparedStatement(ps, o).executeUpdate();
        return ps.getGeneratedKeys();
    }

    public int delete(Connection conn, String query, Object... o) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        return preparePreparedStatement(ps, o).executeUpdate();
    }

    public int update(Connection conn, String query, Object... o) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        return preparePreparedStatement(ps, o).executeUpdate();
    }

    private PreparedStatement preparePreparedStatement(PreparedStatement ps, Object... o) {
        int i = 1;
        for (Object object : o) {
            try {
                if (object == null) {
                    ps.setObject(i, null);
                } else {
                    if (object instanceof String) {
                        ps.setString(i, (String) object);
                    } else if (object instanceof Integer) {
                        ps.setInt(i, (int) object);
                    } else if (object instanceof Long) {
                        ps.setLong(i, (long) object);
                    } else if (object instanceof Double) {
                        ps.setDouble(i, (double) object);
                    } else if (object instanceof Boolean) {
                        ps.setBoolean(i, (boolean) object);
                    } else if (object instanceof Date) {
                        java.sql.Timestamp d = new java.sql.Timestamp(((Date) object).getTime());
                        ps.setTimestamp(i, d);
                    } else if (object instanceof java.sql.Date) {
                        ps.setDate(i, (java.sql.Date) object);
                    } else if (object instanceof byte[]) {
                        ps.setBytes(i, (byte[]) object);
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            i++;
        }
        return ps;
    }

    public void setAutoCommit(Connection conn, boolean autoCommit) throws SQLException {
        conn.setAutoCommit(autoCommit);
    }

    public void commit(Connection conn) throws SQLException {
        conn.commit();
    }

    public void rollback(Connection conn) throws SQLException {
        conn.rollback();
    }

    public String getErrDB() {
        return errDB;
    }

}
