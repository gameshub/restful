/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.wrk;

/**
 *
 * @author myadmin
 */
public interface SQLQuery {

    /* SELECT */
    // Professeur
    String GET_PROFESSEUR_BY_USERNAME = "SELECT * FROM t_user where email = ?";
    String GET_TEACHER_BY_ID = "SELECT * FROM t_user where id = ?";
    String GET_USER_BY_ID = "SELECT * FROM t_user where id = ?";
    String GET_CODE_FROM_USER = "SELECT * FROM t_user where recoverycode = ? and email = ?";
    // Classe
    String GET_CLASS_BY_ID = "SELECT * FROM t_class inner join t_degree on t_degree.id = t_class.degree_id where t_class.id = ?";
    String GET_CLASSES_BY_USER = "SELECT t_class.id, email, password, name, degree, uuid FROM tr_user_class inner join t_user on t_user.id = tr_user_class.user_id inner join t_class on t_class.id = tr_user_class.class_id inner join t_degree on t_degree.id = t_class.degree_id where tr_user_class.user_id = ?";
    String GET_CLASSES = "select * from t_class";
    // Elève
    String GET_STUDENTS_BY_CLASS = "SELECT t_student.id as student_id, t_student.name AS name, t_student.class_id as class_id, t_student.pad_id as student_pad_id, degree, t_class.name AS class, t_class.uuid as class_pad_id FROM t_student INNER JOIN t_class ON t_student.class_id = t_class.id INNER JOIN t_degree ON t_degree.id = t_class.degree_id WHERE t_class.id = ?";
    String GET_STUDENT_BY_ID = "SELECT t_student.id as id, t_student.name as name, t_student.class_id as class_id, degree, t_class.name as class, t_student.pad_id as student_pad_id, t_class.uuid as class_pad_id, t_degree.id as degree_id FROM t_student inner join t_class on t_class.id = t_student.class_id inner join t_degree on t_degree.id = t_class.degree_id WHERE t_student.id = ?";
    String GET_STUDENT_COLLECTION = "SELECT * from t_student WHERE id = ?";
     // Degré
    String GET_DEGREES = "SELECT * FROM t_degree";
    String GET_DEGREES_BY_GAME = "select id from t_degree inner join tr_game_degree on t_degree.id = tr_game_degree.degree_id where tr_game_degree.game_id = ?";
    // Discipline
    String GET_DISCIPLINES = "SELECT * FROM t_discipline";
    // Langues
    String GET_LANGUAGES = "SELECT * FROM t_language";
    String GET_LANGUAGES_BY_GAME = "SELECT t_language.id as id, abbr, t_language.name as name FROM t_translation INNER JOIN t_language ON t_language.id = t_translation.language_id INNER JOIN t_game ON t_game.id = t_translation.game_id WHERE t_game.id = ?";
    // Texts
    String GET_TEXTS_BY_GAME = "SELECT t_translation.translation as translation, t_language.abbr as abbr FROM t_translation INNER JOIN t_language ON t_language.id = t_translation.language_id WHERE t_translation.game_id = ?";
    // Tags
    String GET_TAGS = "select t_tag.name as tag from t_tag";
    String GET_TAGS_AND_GAME = "select t_tag.id as tag_id, t_tag.name as tag, t_game.id as game_id, t_game.name as game from t_tag inner join tr_game_tag on tr_game_tag.tag_id = t_tag.id Inner join t_game on tr_game_tag.game_id = t_game.id";
    String GET_TAGS_BY_GAME = "SELECT t_tag.id as id, t_tag.name as tag FROM t_tag INNER JOIN tr_game_tag on t_tag.id = tr_game_tag.tag_id INNER JOIN t_game on tr_game_tag.game_id = t_game.id where t_game.id = ?";
    // Scenario
    String GET_SCENARIO_BY_GAME = "select t_scenario.json as scenario from t_scenario where t_scenario.game_id = ?";
    
    // Sprites
    String GET_SPRITES_BY_GAME = "SELECT * FROM t_sprite WHERE game_id = ?";
    // Emails
    String GET_EMAILS = "SELECT * FROM t_email";
    // Jeu
    String GET_GAMES = "SELECT * FROM t_game where id ORDER BY id DESC";
    String GET_GAMES_FOR_VALIDATION = "SELECT * FROM t_game";
    String GET_GAME_BY_ID = "SELECT * from t_game where id = ?";
    // Exercice
    String GET_EXERCICE = "SELECT t_exercice.id as id, t_exercice.json as exercice FROM t_exercice INNER JOIN t_degree on t_exercice.degree_id = t_degree.id where t_degree.id = ? AND t_exercice.game_id = ? AND t_exercice.state_id = 2 ORDER BY rand() LIMIT 1";
    String GET_EXERCICE_BY_ID = "SELECT t_exercice.id as id, t_exercice.json as exercice, t_exercice.description as description, t_exercice.game_id as game_id, t_exercice.level as niveau, t_exercice.image as image, t_game.name as game_name, t_exercice.state_id as stateId FROM t_exercice INNER JOIN t_game on t_exercice.game_id = t_game.id where t_exercice.id=?";
    String GET_EXERCICE_STUDENTS = "SELECT t_exercice.id AS id, t_exercice.json AS exercice, t_student.name AS eleve, t_class.name AS classe, t_exercice.date AS date FROM t_exercice INNER JOIN t_student ON t_student.id = t_exercice.student_id INNER JOIN t_class ON t_class.id = t_student.class_id WHERE t_exercice.degree_id = ? AND t_exercice.game_id = ? AND t_exercice.state_id = 1 ORDER BY RAND()";
    String GET_EXERCICE_FOR_VALIDATION = "SELECT t_exercice.id AS pk, t_exercice.json AS exercice, t_student.name AS student, t_class.name AS class, t_class.id AS class_id, t_exercice.date AS date, t_exercice.degree_id AS degree, t_exercice.game_id AS game_id, t_game.name AS game_name FROM t_exercice INNER JOIN t_game ON t_exercice.game_id = t_game.id INNER JOIN t_student ON t_student.id = t_exercice.student_id INNER JOIN t_class ON t_class.id = t_student.class_id WHERE t_exercice.state_id = 1 ORDER BY t_exercice.date";
    String GET_EXERCICES_BY_ID = "SELECT t_exercice.id AS pk, t_exercice.json AS exercice, t_student.name AS eleve, t_class.name AS classe, t_class.id AS classeId, t_exercice.date AS date, t_scenario.json AS scenario , t_exercice.degree_id AS degree FROM t_exercice INNER JOIN t_game ON t_exercice.game_id = t_game.id INNER JOIN t_student ON t_student.id = t_exercice.student_id INNER JOIN t_class ON t_class.id = t_student.class_id LEFT JOIN t_scenario ON t_scenario.game_id = t_game.id WHERE t_exercice.id = ? ORDER BY t_exercice.date";
    String GET_ALL_EXERCICES = "SELECT DISTINCT id, json, description, game_id, state_id, degree_id, level, student_id, image FROM t_exercice";
    String GET_ALL_EXERCICES_BY_PARCOURS_ID = "SELECT DISTINCT id, json, description, game_id, degree_id, level, image FROM t_exercice";
    String GET_EXERCICE_INFO_BY_ID = "SELECT evaluation, status FROM `t_history` WHERE id = 1136";
    
    /*    Parcours      */
    String GET_ALL_PUBLIC_PARCOURS = "SELECT t_parcours.id AS user_id, t_parcours.name AS name, t_parcours.licence AS licence, t_parcours.description AS description, t_parcours.degre_id as degre, t_parcours.competences AS competences, t_parcours.steps AS steps, t_parcours.created AS created, t_parcours.updated AS updated, t_user.email as ownerEmail FROM `t_parcours` inner JOIN t_user on t_parcours.public=2 AND t_user.id=user_id";
    String GET_ALL_SHARED_PARCOURS = "SELECT t_parcours.id AS user_id, t_parcours.name AS name, t_parcours.licence AS licence, t_parcours.description AS description, t_parcours.degre_id as degre, t_parcours.competences AS competences, t_parcours.steps AS steps, t_parcours.created AS created, t_parcours.updated AS updated, t_user.email as ownerEmail FROM `t_parcours` inner JOIN t_user on t_parcours.public=1 AND t_user.id=user_id";
    String GET_PARCOURS_BY_USER_ID = "SELECT * FROM `t_parcours` WHERE user_id = ?";
    // Requete temporaire pour récupérer les données de la vue Parcours_Beta en particulier
//    String GET_PARCOURS_BETA_TRACKS = "SELECT * FROM `parcours_beta_downloaded`";
    String GET_PARCOURS_BETA_TRACKS = "SELECT * FROM `stats_11`";
//    String GET_PARCOURS_BY_USER_ID = "SELECT t_parcours.id, t_parcours.name, t_parcours.licence, t_parcours.description, t_parcours.degre_id, t_parcours.competences, t_parcours.created, t_parcours.updated, t_user.email as ownerEmail FROM `t_parcours` inner JOIN t_user on t_parcours.user_id = 12973 AND t_user.id=?";
    // Pour les affichages des listes de parcours / classe-élève
    String GET_PARCOURS_BY_CLASS_ID = "SELECT t_parcours.id, name FROM tr_class_parcours inner join t_parcours on t_parcours.id = tr_class_parcours.parcours_id where tr_class_parcours.class_id = ?";
    String GET_PARCOURS_BY_STUDENT_ID = "SELECT t_parcours.id, name FROM tr_student_parcours inner join t_parcours on t_parcours.id = tr_student_parcours.parcours_id where tr_student_parcours.student_id = ?";
    // Pour les affichages des checkbox des assignations des parocurs
    String GET_PARCOURS_BY_CLASS_ID_AND_PARCOURS_ID = "SELECT * FROM tr_class_parcours WHERE class_id = ? AND parcours_id = ?";  
    String GET_PARCOURS_BY_STUDENT_ID_AND_PARCOURS_ID = "SELECT * FROM tr_student_parcours WHERE student_id = ? AND parcours_id = ?";
      
    String GET_SINGLE_PARCOURS_BY_USER_AND_NAME = "SELECT * FROM t_parcours WHERE user_id = ? AND name = ?";
    String GET_SINGLE_PARCOURS_BY_ID = "SELECT * FROM t_parcours WHERE id = ?";
    
    String GET_STUDENT_CUSTOM_STEPS_OF_PARCOURS = "SELECT custom_steps FROM tr_student_parcours where student_id = ? and parcours_id = ?";
    // Statistiques
    String GET_STATS_ELEVE = "SELECT t_student.name AS nom_eleve, t_class.name AS nom_classe, t_history.date AS date, t_history.start AS start, t_history.end AS end, t_history.evaluation AS evaluation, t_history.status as status, t_history.exercice_id as exercice_id, t_history.parcours_id as parcours_id, t_class.degree_id AS fk_degre, t_game.id AS pk_jeu, t_game.name AS nom_jeu, t_game.discipline_id AS pk_discipline, t_discipline.name AS nom_discipline FROM t_history INNER JOIN t_student ON t_history.student_id = t_student.id INNER JOIN t_game ON t_history.game_id = t_game.id INNER JOIN t_class ON t_student.class_id = t_class.id INNER JOIN t_discipline ON t_game.discipline_id = t_discipline.id WHERE t_history.student_id = ? AND t_history.status = 1 ORDER BY t_history.end";
    String GET_STATS_ELEVE_PARCOURS = "SELECT t_student.name AS nom_eleve, t_class.name AS nom_classe, t_history.date AS date, t_history.start AS start, t_history.end AS end, t_history.evaluation AS evaluation, t_history.status as status, t_history.exercice_id as exercice_id, t_history.parcours_id as parcours_id, t_class.degree_id AS fk_degre, t_game.id AS pk_jeu, t_game.name AS nom_jeu, t_game.discipline_id AS pk_discipline, t_discipline.name AS nom_discipline FROM t_history INNER JOIN t_student ON t_history.student_id = t_student.id INNER JOIN t_game ON t_history.game_id = t_game.id INNER JOIN t_class ON t_student.class_id = t_class.id INNER JOIN t_discipline ON t_game.discipline_id = t_discipline.id WHERE t_history.student_id = ? AND t_history.status = 1 AND t_history.parcours_id = ? ORDER BY t_history.end;";
    String GET_STATS_CLASSE = "SELECT t_class.name AS nom_classe, t_student.id AS pk_eleve FROM t_history INNER JOIN t_student ON t_history.student_id = t_student.id INNER JOIN t_game ON t_history.game_id = t_game.id INNER JOIN t_class ON t_student.class_id = t_class.id INNER JOIN t_discipline ON t_game.discipline_id = t_discipline.id WHERE t_class.id = ? AND t_history.status = 1 GROUP BY t_student.id";
    String GET_STATS_BY_ELEVE_FOR_GRAPHIC = "SELECT t_history.date AS date, t_history.game_id AS jeu, t_game.NAME AS nomJeu, statsPartieCommence.cnt AS partieCommence, statsPartieTermine.cnt AS partieTermine, stats0.cnt AS count0, stats1.cnt AS count1, stats2.cnt AS count2 FROM t_history INNER JOIN t_game ON t_history.game_id = t_game.id INNER JOIN t_student ON t_student.id = t_history.student_id LEFT JOIN(SELECT date, game_id AS jeu, Count(*) AS cnt FROM t_history WHERE t_history.end IS NULL GROUP BY t_history.date, t_history.game_id) AS statsPartieCommence ON statsPartieCommence.date = t_history.date AND statsPartieCommence.jeu = t_history.game_id LEFT JOIN(SELECT date, game_id AS jeu, Count(*) AS cnt FROM t_history WHERE t_history.end IS NOT NULL GROUP BY t_history.date, t_history.game_id) AS statsPartieTermine ON statsPartieTermine.date = t_history.date AND statsPartieTermine.jeu = t_history.game_id LEFT JOIN(SELECT date, game_id AS jeu, Count(*) AS cnt FROM t_history WHERE t_history.end IS NOT NULL AND t_history.evaluation = 0 GROUP BY t_history.date, t_history.game_id) AS stats0 ON stats0.date = t_history.date AND stats0.jeu = t_history.game_id LEFT JOIN (SELECT date, game_id AS jeu, Count(*) AS cnt FROM t_history WHERE t_history.end IS NOT NULL AND t_history.evaluation = 1 GROUP BY t_history.date, t_history.game_id) AS stats1 ON stats1.date = t_history.date AND stats1.jeu = t_history.game_id LEFT JOIN (SELECT date, game_id AS jeu, Count(*) AS cnt FROM t_history WHERE t_history.end IS NOT NULL AND t_history.evaluation = 2 GROUP BY t_history.date, t_history.game_id) AS stats2 ON stats2.date = t_history.date AND stats2.jeu = t_history.game_id WHERE t_history.student_id = ? AND t_history.mode_id = 2 GROUP BY t_history.game_id, t_history.date ORDER BY t_history.date;";
// Autres
    String COUNT_SHARES_FOR_CLASS= "SELECT COUNT(t_class.id) as sharesCount from tr_user_class inner join t_class on t_class.id = tr_user_class.class_id where t_class.uuid = ? group by t_class.id";
    String COUNT_STEPDONE_IN_PARCOURS_FOR_STUDENT= "SELECT COUNT(*) AS total_exercices FROM t_history WHERE student_id = ? AND parcours_id = ? AND exercice_id = ? AND status = 1";
    //Admin
    String GET_USERS = "Select t_user.id as userId, t_user.email as email, t_user.role_id as roleId, t_role.name as role From t_user Inner join t_role on t_role.id = t_user.role_id";
    String UPDATE_ROLE = "update t_user set t_user.role_id = ? where t_user.id = ?";
    String GET_USER_FROM_ID = "Select t_user.id as userId, t_user.email as email, t_user.role_id as roleId, t_role.name as role From t_user Inner join t_role on t_role.id = t_user.role_id where t_user.id = ?";
    String GET_REPORTING = "SELECT t_exercice.id AS exerciceId, t_reporting.id AS reportingId, t_exercice.json AS exercice, t_user.email AS USER, t_reporting.message AS message, t_class.NAME AS class, t_student.NAME AS student, t_exercice.date AS date, t_degree.degree AS degree, t_exercice.game_id AS gameId FROM t_reporting INNER JOIN t_exercice ON t_exercice.id = t_reporting.exercice_id INNER JOIN t_user ON t_user.id = t_reporting.user_id INNER JOIN t_student ON t_student.id = t_exercice.student_id INNER JOIN t_class ON t_class.id = t_student.class_id INNER JOIN t_degree ON t_degree.id = t_exercice.degree_id WHERE t_exercice.state_id = 3";
    
    
    /* INSERT */

    // Professeur
    String INSERT_USER = "INSERT INTO t_user (email, password, confirmcode, default_student_id) VALUES (?, ?, ?, ?)";
    String INSERT_USER_CLASS = "INSERT INTO tr_user_class (user_id, class_id) values (?, (select id from t_class where uuid=?))";
    // Classe
    String INSERT_CLASS = "INSERT INTO t_class (name, uuid, degree_id) VALUES (?, ?, ?)";
    String INSERT_CLASS_TO_USER = "INSERT INTO tr_user_class (user_id, class_id) VALUES (?, ?)";
    // Elève   
    String INSERT_STUDENT = "INSERT INTO t_student (name, class_id, pad_id) VALUES (?, ?, uuid())";
    // Parcours 
    String CREATE_PARCOURS = "INSERT INTO t_parcours(user_id, name, competences) VALUES (?,?,?)";
    String INSERT_PARCOURS = "INSERT INTO t_parcours(user_id, name, licence, description, degre_id, competences, steps) VALUES (?,?,?,?,?,?,?)";
    String INSERT_PARCOURS_TO_CLASS = "INSERT INTO tr_class_parcours (class_id, parcours_id) VALUES (?,?)";
    String INSERT_PARCOURS_TO_STUDENT = "INSERT INTO tr_student_parcours (student_id, parcours_id, custom_steps) VALUES (?, ?, ?)";
    // Statistiques
    String INSERT_STAT_START = "INSERT INTO t_history (date, start, student_id, game_id, mode_id, exercice_id, parcours_id) values(now(), now(), ?, ?, ?, ?, ?)";
    String INSERT_DAT_STAT_START = "INSERT INTO t_history_dat (student_id, exercice_id, mode_id, parcours_id, dat_function, date, start) VALUES (?, ?, ?, ?, ?, now(), now())";
    // Log
    String INSERT_LOG = "INSERT INTO t_log (Label, Methode, Text) VALUES (?, ?, ?)";
    // Exercice
    String INSERT_EXERCICE = "INSERT INTO t_exercice (json,game_id, student_id, date, state_id, degree_id) values (?,?,?,now(),1,?)";
    String INSERT_UNPLUGGED_ACTIVITY = "INSERT INTO t_exercice (json, game_id, student_id, date, state_id, level) values (?,?,?,now(),3,?)";

    String INSERT_REPORTING = "insert into t_reporting (message, user_id, exercice_id) values (?, ?, ?)";
    
    /* UPDATE */
    
    // Professeur
    String UPDATE_USER_CODE_BY_EMAIL = "UPDATE t_user SET recoverycode = ?, recoveryTime=now() WHERE email = ?";
    String UPDATE_USER_PASSWORD_BY_EMAIL = "UPDATE t_user SET password = ? WHERE email = ?";
    String UPDATE_CONFIRM_USER = "UPDATE t_user SET isConfirmed = 1 WHERE confirmcode = ?";
    // Elève
    String UPDATE_STUDENT = "UPDATE t_student SET name = ? WHERE id = ?";
    String UPDATE_STUDENT_COLLECTION = "UPDATE t_student SET collection = ? WHERE id = ?";
    // Statistiques
    String UPDATE_STAT_START = "UPDATE t_history set t_history.end=now(), t_history.status=1, t_history.evaluation = ?, t_history.details = ? where t_history.id = ? AND t_history.status = 0";
    String UPDATE_DAT_STAT_END = "UPDATE t_history_dat set t_history_dat.end=now(), t_history_dat.status=1, t_history_dat.content= ? where t_history_dat.id = ? and t_history_dat.status = 0";
//    String UPDATE_STAT_START = "UPDATE t_history set t_history.end=now(), t_history.status=1, t_history.evaluation = ? where t_history.id = ?";
    //Parcours
    String UPDATE_PARCOURS = "update t_parcours set licence=?, description=?, steps=? where user_id=? and name=?";
    String UPDATE_PARCOURS_META_DATA = "UPDATE t_parcours SET name=?, description=?, licence=?, degre_id=?, tag=? WHERE id=?";
    String UPDATE_PARCOURS_STATUS = "update t_parcours set public = ? where id = ?";
    String UPDATE_PARCOURS_STEPS_JSON = "update t_parcours set steps = ? where id = ?";
    String UPDATE_CUSTOM_STEPS_JSON = "update tr_student_parcours set custom_steps = ? where student_id = ? and parcours_id = ?";
    String UPDATE_UNPLUGGED_EXERCICE = "UPDATE t_exercice set json = ? WHERE id = ?";

    //Exercices
    String UPDATE_EXERCICE_STATE = "update t_exercice set description = ?, state_id = ?, level = ?, image = ?, metaData_json=? where id = ?";
    String UPDATE_EXERCICE = "update t_exercice set json = ?, level= ? where id = ?";
    
    /* DELETE */
    
    // Parcours     
    String DELETE_PARCOURS_FROM_CLASS_WITHOUT_CLASS_ID = "DELETE FROM tr_class_parcours WHERE parcours_id = ?";
    String DELETE_PARCOURS_FROM_STUDENT_WITHOUT_STUDENT_ID = "DELETE FROM tr_student_parcours WHERE parcours_id = ?";
//    String DELETE_PARCOURS_FROM_USER = "DELETE FROM t_parcours WHERE user_id = ? and id = ?";
    String DELETE_PARCOURS_WITH_ID = "DELETE FROM t_parcours WHERE id = ?";
    
    String DELETE_PARCOURS_FROM_CLASS = "DELETE FROM tr_class_parcours WHERE class_id = ? AND parcours_id = ?";
    String DELETE_PARCOURS_FROM_STUDENT = "DELETE FROM tr_student_parcours WHERE student_id = ? AND parcours_id = ?";

    //Exercice
    String DELETE_EXERCICE_BY_ID = "DELETE from t_exercice where id = ?";
    
    //REPORTING
    String DELETE_REPORTING_BY_EXERCICE_ID = "DELETE from t_reporting where exercice_id = ?";
    String DELETE_HISTORY_BY_EXERCICE_AND_PARCOURS_ID = "DELETE FROM `t_history` WHERE student_id = ? and parcours_id = ?";
}
