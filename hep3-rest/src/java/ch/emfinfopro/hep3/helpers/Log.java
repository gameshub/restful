/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Erwan Sturzenegger
 */
public class Log {

    public static void log(String msg) {
        try {
            //String pathString = System.getProperty("user.dir");
            String pathString = "/home/hep3test/public_html";
            System.out.println(pathString);
            File logFile = new File(pathString + "/logger.log");
            if (!logFile.exists()) {
                logFile.createNewFile();
            }
            
            msg = "[" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date())+"] - " + msg;
            
            BufferedWriter bw = new BufferedWriter(new FileWriter(logFile,true));
            bw.append(msg);
            bw.newLine();
            bw.flush();
            bw.close();
            bw=null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
